BIN_TARGETS=		sz-dhcphook		\
			sz-resolvconf

LIBEXEC_TARGETS=	sz-dhcphook-bound	\
			sz-dhcphook-deconfig	\
			sz-dhcphook-renew

LIBEXEC_TARGETS+=	sz-delroutes		\
			sz-setdefaultroute	\
			sz-setroutes
